package Zaaksysteem::Test::OLO::Message;

use Test::Class::Moose;
use Test::Fatal;
use Test::Mock::One;

use DateTime;
use FindBin;
use Zaaksysteem::OLO::Message;

use FindBin;
use IO::File;

sub test_olo_duplicate_application_id {
    my $xml_file = "$FindBin::Bin/foo.xml";
    my $xml_fh = IO::File->new($xml_file, 'r');

    my $message = Zaaksysteem::OLO::Message->new(
        entry_id  => $xml_file,
        timestamp => DateTime->now,
        content   => join("\n", $xml_fh->getlines)
    );


    isa_ok $message, 'Zaaksysteem::OLO::Message',
        'OLO message constructor retval';

    is $message->application_id, '3513667', "Single application ID found";
}

sub test_olo_invalid_message {
    my $xml_file = "$FindBin::Bin/data/repo/berichten/56391/20170403132157_56391_omvLk01AanbiedenAanvraag.xml";
    my $xml_fh = IO::File->new($xml_file, 'r');

    my $message = Zaaksysteem::OLO::Message->new(
        entry_id => $xml_file,
        timestamp => DateTime->now,
        content => join("\n", $xml_fh->getlines)
    );

    isa_ok $message, 'Zaaksysteem::OLO::Message',
        'OLO message constructor retval';

    ok !$message->is_valid, 'OLO message for different LVO version is not valid';
}

sub test_olo_message_indienen_aanvulling {
    my $xml_file = "$FindBin::Bin/data/repo/berichten/2598177/20170129160023_2598177_vrgDi01IndienenAanvulling.xml";
    my $xml_fh = IO::File->new($xml_file, 'r');

    my $message = Zaaksysteem::OLO::Message->new(
        entry_id => $xml_file,
        timestamp => DateTime->now,
        content => join("\n", $xml_fh->getlines)
    );

    isa_ok $message, 'Zaaksysteem::OLO::Message',
        'OLO message constructor retval';

    subtest 'message parser checks' => sub {
        is $message->application_id, '2598177',
            'application_id returns expected value';

        is $message->stuf_type, 'IndienenAanvulling',
            'stuf_type returns expected value';

        is $message->stuf_reference, '6418126',
            'stuf_reference returns expected value';
    };

    ok !defined $message->location, 'message location not found';
    ok !defined $message->requestor, 'message application requestor not found';

    is_deeply $message->application_attributes, {
        aanvraagnummer => '2598177'
    }, 'message application attributes parsed';

    is_deeply [ $message->attachment_paths ], [
        '/2598177/basisset_aanvraag/2598177_1485701965134_constructieberekening.pdf'
    ], 'message attachments found';
}

sub test_olo_message_aanbieden_aanvraag {
    my $xml_file = "$FindBin::Bin/data/repo/berichten/2817104/20170220175947_2817104_omvDi01AanbiedenAanvraag.xml";
    my $xml_fh = IO::File->new($xml_file, 'r');

    my $message = Zaaksysteem::OLO::Message->new(
        entry_id => $xml_file,
        timestamp => DateTime->now,
        content => join("\n", $xml_fh->getlines)
    );

    isa_ok $message, 'Zaaksysteem::OLO::Message',
        'OLO message constructor retval';

    subtest 'message parser check' => sub {
        is $message->application_id, '2817104',
            'application_id returns expected value';

        is $message->stuf_type, 'AanbiedenAanvraag',
            'stuf_type returns expected value';

        is $message->stuf_reference, '6584064',
            'stuf_reference returns expected value';
    };

    is_deeply $message->location, {
        city => 'Testerdam',
        street => 'Teststraat',
        street_number => '123',
        postal_code => '9999XP'
    }, 'message location parsed';

    is_deeply $message->requestor, {
        type => 'person',
        id => '123546789'
    }, 'message application requestor parsed';

    is_deeply $message->application_attributes, {
		aanvraagNaam => 'Verlagen trottoir tbv bestaande oprit',
		aanvraagProcedure => 'Reguliere procedure',
		aanvraagStatus => 'In behandeling',
		aanvraagdatum => '20170220',
		aanvraagnummer => '2817104',
		gedeeltelijkGoedkeurenGewenst => 'nee',
		gefaseerdIndienen => 'niet',
		landelijkeFormulierVersie => '2017.01',
		omschrijvingNietInTeDienenBijlagen => 'Het betreft slechts het verlagen van het trottoir',
		omschrijvingUitgesteldeBijlagen => 'Het betreft slechts het verlagen van het trottoir',
		persoonsgegevensVrijgeven => 'ja',
		plichtStatus => 'Vergunningsplicht',
		projectKosten => '300',
		projectOmschrijving => 'Bij bestaande oprit het trottoir verlagen over een lengte van 3 meter',
		toelichtingBijIndiening => '-',
		uitgesteldeAanleveringBijlagen => 'ja'
    }, 'message application attributes parsed';

    is_deeply [ $message->attachment_paths ], [
        '/2817104/basisset_aanvraag/2817104_1487609987099_papierenformulier.pdf',
        '/2817104/basisset_aanvraag/2817104_1487609987108_publiceerbareaanvraag.pdf',
    ], 'message attachments found';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE> file.
