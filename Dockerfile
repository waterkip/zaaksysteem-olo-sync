FROM registry.gitlab.com/zaaksysteem/zaaksysteem-service-http-library:d190b61a as build

ENV SERVICE_HOME /opt/zaaksysteem-olo-sync-service
WORKDIR $SERVICE_HOME

COPY dev-bin ./dev-bin

COPY Makefile.PL ./

RUN ./dev-bin/cpanm --installdeps --test-only . \
    && ./dev-bin/cpanm --installdeps -n .

FROM build as test

COPY . .

RUN prove -lv


FROM test as prod


ENV ZSVC_LOG4PERL_CONFIG=/etc/zaaksysteem-olo-sync-service/log4perl.conf \
    ZSVC_SERVICE_CONFIG=/etc/zaaksysteem-olo-sync-service/zaaksysteem-olo-sync-service.conf

COPY etc /etc/zaaksysteem-olo-sync-service

CMD starman $SERVICE_HOME/bin/zaaksysteem-olo-sync-service.psgi
